
# 崩溃分析
arm-none-eabi-objdump -d nuttx_coolfly-v1_default.elf > ~/Desktop/log

up_registerdump: R0: ffffffe9 2000f180 2000a3f0 ffffffe9 ffffffe9 2000f184 2000a3f0 00000000
up_registerdump: R8: 200009d4 00000039 2000f710 00000000 00000000 2000f5f8 1002b58b 1002c574

R15: 1002c574 表示崩溃的原因
R14: 1002b58b - 1 表示跳转到R15的命令


# 查看 USB 的方式

lsusb: 可以查看你的USB 情况

ls /dev 可以看到你具体的设备的情况


# 杀死 defunct 的进程

ps -ef | grep defunct | more
